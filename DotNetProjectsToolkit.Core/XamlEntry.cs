﻿using FEx.Basics.Abstractions;
using FlakEssentials.MSBuild;
using System.IO;

namespace DotNetProjectsToolkit.Core;

public class XamlEntry : NotifyPropertyChanged
{
    public MSProject Project { get; }
    public string Name { get; }
    public string RelevantResx { get; }
    public string RelevantResxRelative { get; }
    public string RelevantResxDesigner { get; }
    public string RelevantResxDesignerRelative { get; }
    public string ProjectFilePath => Project.FullPath;
    public string ProjectDirectoryPath => Project.ProjectDir;
    public string AbsolutePath { get; }
    public string RelativePath { get; }
    public string ProjectNamespace => Project.AssemblyName;

    public XamlEntry(string filePath, MSProject project)
    {
        RelativePath = filePath;
        Name = Path.GetFileNameWithoutExtension(RelativePath);
        Project = project;

        RelevantResx = Path.Combine(ProjectDirectoryPath, "Properties", $"{Name}.resx");
        RelevantResxRelative = Path.Combine("Properties", $"{Name}.resx");
        RelevantResxDesigner = Path.Combine(ProjectDirectoryPath, "Properties", $"{Name}.Designer.cs");
        RelevantResxDesignerRelative = Path.Combine("Properties", $"{Name}.Designer.cs");
        AbsolutePath = Path.Combine(ProjectDirectoryPath, RelativePath);
    }
}