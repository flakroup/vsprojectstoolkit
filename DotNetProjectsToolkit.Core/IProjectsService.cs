using FEx.Basics.Collections.Concurrent;
using FEx.MVVM.Abstractions.Interfaces;
using FlakEssentials.MSBuild;
using FlakEssentials.NuGetEx;
using FlakEssentials.XamlStyler;
using NuGet.Protocol.Core.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DotNetProjectsToolkit.Core;

public interface IProjectsService : IProgressAggregator
{
    event EventHandler<string> XamlContentChanged;
    event EventHandler<EventArgs> DataLoaded;

    List<string> Tags { get; }
    List<string> Excludes { get; }
    List<string> FileItemsNames { get; }
    FileInfo Cfg { get; }
    XamlStylerCommon XamlStylerCommonSrv { get; }
    NuGetManager NuGetMgr { get; }
    ConcurrentObservableList<XamlString> DataGridItemsSource { get; }
    string XamlContent { get; set; }
    bool DataGridIsBusy { get; set; }
    ConcurrentObservableList<XamlFile> XamlFiles { get; }
    ConcurrentObservableList<DiffResult> Diff { get; }
    ConcurrentObservableList<string> ExcludedPaths { get; }
    MSSolution Solution { get; set; }
    MSProject Project { get; set; }
    string Mask { get; set; }
    ITreeHandler TreeHandler { get; }

    Task<string[]> ReformatAllXamlsAsync();
    Task ExportAsync();
    void ShowXaml(XamlString curr);
    Task StartAsync(string fileName);
    Task OpenSolutionAsync(string solutionFilePath);
    void Rebind();
    void AddMask();
    void DeleteSelectedFiles(List<DiffResult> selectedResults);
    Task<(DownloadResourceResult result, bool isSuccess)> PrepareXamlStylerConsolePackageAsync();
    Task PrepareXamlStylerConsoleAsync();
    Task<bool> ReformatAllCsFilesAsync();
}