﻿using FlakEssentials.MSBuild;

namespace DotNetProjectsToolkit.Core;

public class XamlResult : XamlFile
{
    public bool IsDesignerIncluded { get; set; }
    public bool IsResxIncluded { get; set; }

    public XamlResult(MSProjectItem xaml, MSProject project)
        : base(xaml, project)
    {
    }
}