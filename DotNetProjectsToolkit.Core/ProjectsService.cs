using FEx.Abstractions.Interfaces;
using FEx.Basics.Collections.Concurrent;
using FEx.Basics.Extensions;
using FEx.Common.Abstractions.Interfaces;
using FEx.Common.Extensions;
using FEx.Common.Helpers;
using FEx.Extensions;
using FEx.Extensions.Collections.Lists;
using FEx.Extensions.DateTimes;
using FEx.Extensions.IO;
using FEx.Fundamentals.Utilities;
using FEx.MVVM.Utilities;
using FlakEssentials.Extensions.Utilities;
using FlakEssentials.MSBuild;
using FlakEssentials.MSBuild.Extensions;
using FlakEssentials.NuGetEx;
using FlakEssentials.XamlStyler;
using Microsoft.Build.Construction;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.CodeAnalysis.Text;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using Formatting = Newtonsoft.Json.Formatting;

namespace DotNetProjectsToolkit.Core;

public class ProjectsService : ProgressAggregator, IProjectsService
{
    private readonly IResxManager _resxManager;
    private readonly IAppInfoProvider _appInfoProvider;

    private string _xamlContent;
    private bool _dataGridIsBusy;
    private string _mask;
    private MSSolution _solution;
    private MSProject _project;

    public event EventHandler<string> XamlContentChanged;
    public event EventHandler<EventArgs> DataLoaded;
    public ITreeHandler TreeHandler { get; }

    public List<string> Tags { get; } =
    [
        "Text",
        "Content",
        "Header",
        "WatermarkContent",
        "Label",
        "ToolTip",
        "Title"
    ];

    public List<string> Excludes { get; } =
    [
        "{Binding[^}]*",
        "{MultiBinding[^}]*",
        "{x:Static[^}]*}",
        "{TemplateBinding[^}]*}"
    ];

    public List<string> FileItemsNames { get; } =
    [
        "_DeploymentManifestIconFile",
        "ApplicationDefinition",
        "Compile",
        "Content",
        "EmbeddedResource",
        "None",
        "Page",
        "Resource",
        "SplashScreen",
        "UpToDateCheckInput"
    ];

    public FileInfo Cfg { get; }
    public XamlStylerCommon XamlStylerCommonSrv { get; }
    public NuGetManager NuGetMgr { get; }
    public ConcurrentObservableList<XamlString> DataGridItemsSource { get; }

    public string XamlContent
    {
        get => _xamlContent;
        set => SetProperty(ref _xamlContent, value, v => XamlContentChanged?.Invoke(this, v));
    }

    public bool DataGridIsBusy
    {
        get => _dataGridIsBusy;
        set => SetProperty(ref _dataGridIsBusy, value);
    }

    public ConcurrentObservableList<XamlFile> XamlFiles { get; }
    public ConcurrentObservableList<DiffResult> Diff { get; }
    public ConcurrentObservableList<string> ExcludedPaths { get; }

    public MSSolution Solution
    {
        get => _solution;
        set => SetProperty(ref _solution, value);
    }

    public MSProject Project
    {
        get => _project;
        set => SetProperty(ref _project, value);
    }

    public string Mask
    {
        get => _mask;
        set => SetProperty(ref _mask, value);
    }

    private static Dictionary<string, string> InvalidChars { get; } = new()
    {
        ["~"] = "Tilde",
        ["`"] = "Accent",
        ["!"] = "Exclamation",
        ["@"] = "At",
        ["#"] = "Hash",
        ["$"] = "Dollar",
        ["%"] = "Percent",
        ["^"] = "Dash",
        ["&"] = "Amp",
        ["*"] = "Star",
        ["("] = "OpenBracket",
        [")"] = "CloseBracket",
        ["-"] = "_",
        ["+"] = "Plus",
        ["="] = "Equals",
        ["["] = "OpenSqBracket",
        ["{"] = "OpenCurlyBracket",
        ["]"] = "CloseSqBracket",
        ["}"] = "CloseCurlyBracket",
        ["\\"] = "Backslash",
        ["|"] = "VBar",
        ["'"] = "Apostrophe",
        ["\""] = "Quot",
        [";"] = "Semicolon",
        [":"] = "Colon",
        ["/"] = "Slash",
        ["?"] = "Question",
        ["."] = "",
        [">"] = "RArrow",
        [","] = "Comma",
        ["<"] = "LArrow",
        ["©"] = "Copyright",
        [" "] = "Space"
    };

    public ProjectsService(NuGetManager nuGetManager,
                           XamlStylerCommon xamlStylerCommonSrv,
                           ITreeHandler treeHandler,
                           IResxManager resxManager,
                           IAppInfoProvider appInfoProvider)
    {
        TreeHandler = treeHandler;
        _resxManager = resxManager;
        _appInfoProvider = appInfoProvider;
        DataGridItemsSource = [];
        XamlFiles = [];
        XamlContent = string.Empty;
        DataGridIsBusy = false;
        Diff = [];
        ExcludedPaths = [];
        NuGetMgr = nuGetManager;
        XamlStylerCommonSrv = xamlStylerCommonSrv;
        Cfg = appInfoProvider.AppData.GetDescendantFile("XamlStyler.config.json");
    }

    public async Task<string[]> ReformatAllXamlsAsync()
    {
        PrgSet(0, XamlFiles.Count);
        SetStatusInfo("Formatting XAML files");
        string[] result = await XamlFiles.RunWithWhenAllAsync(xaml => ReformatXamlFile(xaml));
        SetStatusInfo($"Done in {Stopwatch.GetTime()}");
        PrgSetEnd();

        return result;
    }

    public async Task ExportAsync()
    {
        var entries = new Dictionary<string, XamlResult>();

        try
        {
            SetStatusInfo("Preparing update list");
            PrgSetMax(DataGridItemsSource.Count);

            foreach (XamlString entry in DataGridItemsSource)
            {
                if (!entries.ContainsKey(entry.XamlEntry.AbsolutePath))
                    entries.Add(entry.XamlEntry.AbsolutePath, (XamlResult)entry.XamlEntry);

                entries[entry.XamlEntry.AbsolutePath].Strings.Add(entry);
                PrgAdd();
            }

            SetStatusInfo("Updating RESX files");
            PrgSetMax(entries.Count);

            foreach (KeyValuePair<string, XamlResult> xaml in entries)
            {
                var data = xaml.Value.Strings.Select(xamlString =>
                        new KeyValuePair<string, string>(CreateKey(xamlString.Text), xamlString.Text))
                    .ToList();

                if (data.Count > 0)
                    _resxManager.UpdateResourceFile(data, xaml.Value.RelevantResx);

                PrgAdd();
            }

            SetStatusInfo("Updating XAML files");
            var files = new List<XamlResult>(); //entries.Select(x => x.Value).Distinct().ToList();
            PrgSetMax(entries.Count);

            foreach (KeyValuePair<string, XamlResult> entry in entries)
            {
                Dictionary<string, string> resourceEntries = _resxManager.GetResourcesEntries(entry.Value.RelevantResx);

                if (resourceEntries.Count > 0
                    && UpdateXamlFile(resourceEntries, entry.Value))
                    files.Add(entry.Value); //todo multithread

                PrgAdd();
            }

            if (files.Count > 0)
                UpdateProjectFile(files);
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }

        if (entries.Count > 0)
        {
            MSProject project = entries.FirstOrDefault().Value.Project;
            await OpenProjectAsync(project);
        }
    }

    public void ShowXaml(XamlString curr)
    {
        XamlFile xaml = XamlFiles.FirstOrDefault(x => x.AbsolutePath == curr?.XamlEntry?.AbsolutePath);

        if (xaml != null)
            Dispatcher.InvokeOnMainThread(() =>
            {
                //_scintilla.//todo syntax highlighting and lines enumeration
                XamlContent = xaml.XamlContent;
                //int pos = mainWindow.textBox.GetCharacterIndexFromLineIndex(curr.LineNr);
                //if (pos > -1)
                //{
                //    mainWindow.textBox.Select(pos, 0);
                //}
            });
        else
            XamlContent = string.Empty;
    }

    public async Task StartAsync(string fileName)
    {
        if (fileName.IsNullOrEmpty())
            return;

        try
        {
            XamlContent = string.Empty;
            SetIsIndeterminate(true);
            DataGridIsBusy = true;
            Solution = null;

            switch (Path.GetExtension(fileName))
            {
                case ".csproj":
                    await OpenProjectAsync(fileName, SolutionProjectType.KnownToBeMSBuildFormat);

                    break;
                case ".sln":
                    await OpenSolutionAsync(fileName);

                    break;
            }

            DataLoaded?.Invoke(this, EventArgs.Empty);

            PrgSetEnd();
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }
    }

    public async Task OpenSolutionAsync(string solutionFilePath)
    {
        string solutionDirectoryPath = Path.GetDirectoryName(solutionFilePath);

        if (!string.IsNullOrEmpty(solutionDirectoryPath))
        {
            Solution = new(solutionFilePath);
            await Solution.InitializeAsync();
            ExcludedPaths.Clear();
            Diff.Clear();
            Project = null; //todo should we remove current project?
            PrgSetMax(Solution.Projects.Count);
            await TreeHandler.OpenSolutionAsync(Solution);
            XamlFiles.Clear();

            //await Task.WhenAll(Solution.Projects.Select(project => OpenProjectAsync(project, false)).ToArray());
            foreach (MSProject project in Solution.Projects)
                await OpenProjectAsync(project, false);

            if (XamlFiles.Count > 0)
                PrepareResults();

            //List<string> types = Solution.Projects.SelectMany(x => x.Project.AllEvaluatedItems.Where(y => x.Files.Contains(y.EvaluatedInclude)).Select(z => z.ItemType)).Distinct().OrderBy(x => x).ToList();
        }
    }

    public void Rebind()
    {
        PrgSetMax(XamlFiles.Count);

        foreach (XamlFile file in XamlFiles)
        {
            try
            {
                Dictionary<string, string> resourceEntries = _resxManager.GetResourcesEntries(file.RelevantResx);

                if (resourceEntries.Count > 0)
                {
                    string content = File.ReadAllText(file.AbsolutePath).Trim();
                    var sb = new StringBuilder(content);
                    string resxName = Path.GetFileNameWithoutExtension(file.RelevantResx);

                    foreach (KeyValuePair<string, string> entry in resourceEntries)
                    {
                        var value = $"=\"{entry.Value.Replace("<", "&lt;").Replace(">", "&gt;")}\"";
                        sb = sb.Replace("=\"{x:Static properties:" + resxName + "." + entry.Key + "}\"", value);
                    }

                    Encoding enc = GetEncoding(file);
                    File.WriteAllText(file.AbsolutePath, sb.ToString(), enc);
                    UpdateXamlFile(resourceEntries, file);
                }
            }
            catch (Exception ex)
            {
                ex.HandleException(false);
            }

            PrgAdd();
        }
    }

    public void AddMask()
    {
        var filesPaths = new List<string>();
        filesPaths.AddRange(Diff.Where(x => Regex.Match(x.FullPath, Mask).Success).Select(x => x.FullPath));
        DiffResult[] temp = Diff.Where(x => filesPaths.Contains(x.FullPath)).ToArray();

        foreach (DiffResult diff in temp)
            Diff.Remove(diff);

        ExcludedPaths.AddUnique(Mask);
        Mask = string.Empty;
    }

    public void DeleteSelectedFiles(List<DiffResult> selectedResults)
    {
        PrgSetMax(selectedResults.Count);

        foreach (DiffResult res in selectedResults)
        {
            try
            {
                res.Info.Delete();
                Diff.Remove(res);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
            finally
            {
                PrgAdd();
            }
        }
    }

    public async Task<(DownloadResourceResult result, bool isSuccess)> PrepareXamlStylerConsolePackageAsync()
    {
        await NuGetMgr.InitializeAsync();

        return await NuGetMgr.RestorePackageByIdAsync("XamlStyler.Console");
    }

    public async Task PrepareXamlStylerConsoleAsync()
    {
        Task<(DownloadResourceResult result, bool isSuccess)> task = PrepareXamlStylerConsolePackageAsync();

        string str =
            _appInfoProvider.EntryAssembly.ReadEmbeddedFile("XamlStyler.config.json", addNamespace: true);

#if NETFRAMEWORK
        File.WriteAllText(Cfg.FullName, str);
#else
        await File.WriteAllTextAsync(Cfg.FullName, str);
#endif
        (DownloadResourceResult result, bool isSuccess) = await task;

        if (!isSuccess)
        {
            XamlStylerCommon.XStyler = null;
        }
        else
        {
            DirectoryInfo root = new FileInfo(result.PackageReader.GetNuspecFile()).Directory;
            string xs = result.PackageReader.GetFiles().Single(x => x.EndsWith("/xstyler.dll"));
            XamlStylerCommon.XStyler = root.GetDescendantFile(xs.Split('/'));
        }
    }

    public async Task<bool> ReformatAllCsFilesAsync()
    {
        Document[] files = null;

        using (var workspace = MSBuildWorkspace.Create())
        {
            if (Solution != null)
            {
                Solution sln = await workspace.OpenSolutionAsync(Solution.SolutionFilePath);
                files = sln.Projects.SelectMany(x => x.Documents).ToArray();
            }
            else if (Project != null)
            {
                Project project = await workspace.OpenProjectAsync(Project.FullPath);
                files = project.Documents.ToArray();
            }

            if (files.IsNotNullOrEmptyList())
            {
                PrgSetMax(files.Length);
                await files.RunWithWhenAllTasksAsync(FormatCsFileAsync);

                return workspace.TryApplyChanges(workspace.CurrentSolution);
            }
        }

        return false;
    }

    // ReSharper disable UnusedParameter.Global
    protected static void ValidateResults(List<XamlString> results)
    // ReSharper restore UnusedParameter.Global
    {
        //todo check for existing resources and update results
    }

    private static Encoding GetEncoding(XamlFile file)
    {
        Encoding enc = Encoding.UTF8;

        if (File.Exists(file.AbsolutePath))
        {
            File.SetAttributes(file.AbsolutePath, FileAttributes.Normal);
            enc = FileSystemUtilities.GetEncoding(file.AbsolutePath);
        }

        return enc;
    }

    private static string ReplaceInvalidChars(string s)
    {
        string result = s;

        foreach (KeyValuePair<string, string> invalidChar in InvalidChars)
        {
            int idx = result.IndexOf(invalidChar.Key, StringComparison.Ordinal) + 1;

            if (idx > 0)
            {
                if (idx < result.Length)
                    result = result.Substring(0, idx) + result[idx].ToString().ToUpper() + result.Substring(idx + 1);

                result = result.Replace(invalidChar.Key, invalidChar.Value);
            }
        }

        return result;
    }

    private static string CreateKey(string strText)
    {
        strText = strText.Trim().ToLower();

        while (strText.Length > 1
               && (strText[strText.Length - 1] == '.' || strText[strText.Length - 1] == ':'))
        {
            strText = strText.Substring(0, strText.Length - 1);
            strText = strText.Trim();
        }

        strText = ReplaceInvalidChars(strText);
        var res = strText.Split(' ').ToList();

        while (res.Contains(string.Empty))
            res.Remove(string.Empty);

        for (var i = 0; i < res.Count; i++)
            res[i] = res[i][0].ToString().ToUpper() + res[i].Substring(1);

        int count = res.Count > 5
            ? 5
            : res.Count;

        return string.Join(string.Empty, res.GetRange(0, count));
    }

    private void UpdateProjectFile(IReadOnlyCollection<XamlResult> files)
    {
        //string excludeBy = ".Designer.cs";
        SetStatusInfo("Generating designer files");
        var projects = files.Select(x => x.ProjectFilePath).Distinct().ToList();
        PrgSetMax(files.Count);

        foreach (XamlResult file in files)
        {
            string[] unmatched = _resxManager.Resgen(file.RelevantResx,
                file.RelevantResxDesigner,
                file.Name,
                file.ProjectNamespace);

            if (unmatched.Length > 0)
            {
                //todo revert;
            }

            PrgAdd();
        }

        PrgSetEnd();
        SetStatusInfo("Updating projects files");
        PrgSetMax(projects.Count);

        foreach (string project in projects)
        {
            var projectXamls = files.Where(x => x.ProjectFilePath == project).ToList();
            IXmlLineInfo xmlInfo = null;

            try
            {
                string projectXmlContent = File.ReadAllText(project);
                string projectName = Path.GetFileNameWithoutExtension(project);
                SetCurrItemInfo(projectName);

                using (var stringReader = new StringReader(projectXmlContent))
                {
                    var output = new StringBuilder();

                    using (var stringWriter = new ExtendedStringWriter(output, true, new UTF8Encoding(true)))
                    {
                        using (var reader = XmlReader.Create(stringReader))
                        {
                            using (var writer = XmlWriter.Create(stringWriter,
                                       new()
                                       {
                                           Indent = true,
                                           Encoding = new UTF8Encoding(true),
                                           CloseOutput = true,
                                           WriteEndDocumentOnClose = true
                                       }))
                            {
                                int compileParentDepth = -1;
                                int embeddedResourceParentDepth = -1;
                                var designersDumped = false;
                                var embeddedDumped = false;

                                while (reader.Read())
                                {
                                    xmlInfo = (IXmlLineInfo)reader;

                                    switch (reader.NodeType)
                                    {
                                        case XmlNodeType.Element:
                                            if (reader.Name == "Compile")
                                            {
                                                if (compileParentDepth == -1)
                                                    compileParentDepth = reader.Depth - 1;

                                                string include = reader.GetAttribute("Include");

                                                if (include != null
                                                    && projectXamls.Any(x => x.RelevantResxDesignerRelative == include))
                                                    projectXamls[projectXamls.IndexOf(projectXamls.First(x =>
                                                            x.RelevantResxDesignerRelative == include))]
                                                        .IsDesignerIncluded = true;
                                            }
                                            else if (reader.Name == "EmbeddedResource")
                                            {
                                                if (embeddedResourceParentDepth == -1)
                                                    embeddedResourceParentDepth = reader.Depth - 1;

                                                string include = reader.GetAttribute("Include");

                                                if (include != null
                                                    && projectXamls.Any(x => x.RelevantResxRelative == include))
                                                    projectXamls[projectXamls.IndexOf(projectXamls.First(x =>
                                                        x.RelevantResxRelative == include))].IsResxIncluded = true;
                                            }

                                            string xmlns = reader.GetAttribute("xmlns");

                                            if (xmlns == null)
                                                writer.WriteStartElement(reader.Name);
                                            else
                                                writer.WriteStartElement(reader.Name, xmlns);

                                            writer.WriteAttributes(reader, true);

                                            if (reader.IsEmptyElement)
                                                writer.WriteEndElement();

                                            break;
                                        case XmlNodeType.Text:
                                            writer.WriteString(reader.Value);

                                            //todo if embeded && !PubGen => PubGen
                                            break;
                                        case XmlNodeType.Whitespace:
                                            break;
                                        case XmlNodeType.CDATA:
                                            writer.WriteCData(reader.Value);

                                            break;
                                        case XmlNodeType.XmlDeclaration:
                                        case XmlNodeType.ProcessingInstruction:
                                            writer.WriteProcessingInstruction(reader.Name, reader.Value);

                                            break;
                                        case XmlNodeType.Comment:
                                            writer.WriteComment(reader.Value);

                                            break;
                                        case XmlNodeType.EndElement:
                                            if (reader.Name == "Project"
                                                && !designersDumped
                                                && reader.Depth == 0)
                                            {
                                                writer.WriteStartElement("ItemGroup");

                                                foreach (XamlResult file in projectXamls)
                                                {
                                                    if (!file.IsDesignerIncluded
                                                        && File.Exists(file.RelevantResxDesigner))
                                                    {
                                                        writer.WriteStartElement("Compile");

                                                        writer.WriteAttributeString("Include",
                                                            file.RelevantResxDesignerRelative);

                                                        writer.WriteStartElement("AutoGen");
                                                        writer.WriteString("True");
                                                        writer.WriteFullEndElement();
                                                        writer.WriteStartElement("DesignTime");
                                                        writer.WriteString("True");
                                                        writer.WriteFullEndElement();
                                                        writer.WriteStartElement("DependentUpon");
                                                        writer.WriteString(Path.GetFileName(file.RelevantResx));
                                                        writer.WriteFullEndElement();
                                                        writer.WriteFullEndElement();
                                                    }
                                                }

                                                writer.WriteFullEndElement();
                                                designersDumped = true;
                                            }

                                            if (reader.Name == "Project"
                                                && !embeddedDumped
                                                && reader.Depth == 0)
                                            {
                                                writer.WriteStartElement("ItemGroup");

                                                foreach (XamlResult file in projectXamls)
                                                {
                                                    if (!file.IsResxIncluded
                                                        && File.Exists(file.RelevantResx))
                                                    {
                                                        writer.WriteStartElement("EmbeddedResource");

                                                        writer.WriteAttributeString("Include",
                                                            file.RelevantResxRelative);

                                                        writer.WriteStartElement("Generator");
                                                        writer.WriteString("PublicResXFileCodeGenerator");
                                                        writer.WriteFullEndElement();
                                                        writer.WriteStartElement("LastGenOutput");
                                                        writer.WriteString(file.RelevantResxDesignerRelative);
                                                        writer.WriteFullEndElement();
                                                        writer.WriteFullEndElement();
                                                    }
                                                }

                                                writer.WriteFullEndElement();
                                                embeddedDumped = true;
                                            }

                                            writer.WriteFullEndElement();

                                            break;
                                        case XmlNodeType.None:
                                            break;
                                        case XmlNodeType.Attribute:
                                            break;
                                        case XmlNodeType.EntityReference:
                                            break;
                                        case XmlNodeType.Entity:
                                            break;
                                        case XmlNodeType.Document:
                                            break;
                                        case XmlNodeType.DocumentType:
                                            break;
                                        case XmlNodeType.DocumentFragment:
                                            break;
                                        case XmlNodeType.Notation:
                                            break;
                                        case XmlNodeType.SignificantWhitespace:
                                            break;
                                        case XmlNodeType.EndEntity:
                                            break;
                                    }
                                }

                                reader.Close();
                                writer.Close();
                                File.WriteAllText(project, output.ToString(), new UTF8Encoding(true));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HandleException(custom: ("xmlInfo", xmlInfo.ToJson(Formatting.Indented)));
            }
        }
    }

    private bool UpdateXamlFile(Dictionary<string, string> resourceEntries, XamlFile file)
    {
        string resxName = Path.GetFileNameWithoutExtension(file.RelevantResx);

        try
        {
            if (resourceEntries.Count > 0)
            {
                string content = File.ReadAllText(file.AbsolutePath).Trim();

                string xmlnsEntry = " xmlns:properties=\"clr-namespace:" + file.ProjectNamespace + ".Properties\"\r\n";

                var sb = new StringBuilder(content);

                if (!content.Contains(xmlnsEntry)) //todo add xmlns alias detection
                {
                    int xmlnsIdx = content.IndexOf(" ", StringComparison.Ordinal);

                    if (xmlnsIdx > -1)
                        sb = sb.Insert(xmlnsIdx, xmlnsEntry);
                }

                foreach (KeyValuePair<string, string> entry in resourceEntries)
                {
                    foreach (string tag in Tags)
                    {
                        var value = $"{tag}=\"{entry.Value.Replace("<", "&lt;").Replace(">", "&gt;")}\"";

                        sb = sb.Replace(value, tag + "=\"{x:Static properties:" + resxName + "." + entry.Key + "}\"");
                    }
                }

                Encoding enc = GetEncoding(file);
                //int cnt = -1;
                //string pfx = string.Empty;
                //while (File.Exists(path + ".bak" + pfx))
                //{
                //    cnt++;
                //    pfx = cnt.ToString();
                //}
                //File.Copy(path, path + ".bak" + pfx);
                File.WriteAllText(file.AbsolutePath, sb.ToString(), enc);
                ReformatXamlFile(file, false);

                return true;
            }
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }

        return false;
    }

    private string ReformatXamlFile(XamlFile xaml, bool reportProgress = true)
    {
        //todo ensure backup creation and restore
        string log = XamlStylerCommon.ReformatXamlFile(xaml.AbsolutePath, Cfg.FullName);

        if (reportProgress)
        {
            SetCurrItemInfo(xaml.RelativePath);
            PrgAdd();
        }

        return log;
        //todo json to selectable resource
    }

    private async Task OpenProjectAsync(string projectFile,
                                        SolutionProjectType solutionProjectType,
                                        bool isSingleProject = true,
                                        string solutionPackagesDir = null)
    {
        XamlFiles.Clear();

        await OpenProjectAsync(MSBuildExtensions.FromFile(projectFile, solutionProjectType, solutionPackagesDir),
            isSingleProject);

        if (XamlFiles.Count > 0)
            PrepareResults();
    }

    private async Task OpenProjectAsync(MSProject project, bool isSingleProject = true)
    {
        //todo prepare to open projects of solution
        if (project != null)
        {
            SetStatusInfo(project.Name);

            XamlFile[] xamls = project.IncludedFiles.Where(x => x.Extension.IsEqual(".xaml"))
                .Select(xaml => new XamlFile(xaml, project))
                .ToArray();

            if (xamls.Length > 0)
            {
                await xamls.RunWithWhenAllTasksAsync(x => GetXamlContentAsync(x));
                XamlFiles.AddRange(xamls);
            }

            //Dictionary<string, List<MSProjectItem>> re = projectFiles.GroupBy(x => x.ItemType).ToDictionary(x => x.Key, x => x.ToList());
            if (isSingleProject)
            {
                Diff.Clear();
                ExcludedPaths.Clear();
            }

            await TreeHandler.BuildFilesTreeAsync(project.IncludedFiles, Solution, project, isSingleProject);
            Diff.AddRange(project.IgnoredFiles);
            Diff.AddRange(project.MissingFiles);
            ExcludedPaths.AddUniqueRange(project.IgnoredDirectories);
        }

        PrgAdd();
    }

    private void PrepareResults()
    {
        Dispatcher.InvokeOnMainThread(() => DataGridItemsSource.Clear());

        //merge results
        var results = XamlFiles.SelectMany(xaml => xaml.Strings,
                (xaml, str) => new XamlString(xaml)
                {
                    Text = str.Text,
                    LineNr = str.LineNr,
                    Tag = str.Tag,
                    Valid = str.Valid,
                    Key = CreateKey(str.Text)
                })
            .ToList();

        ValidateResults(results);

        Dispatcher.InvokeOnMainThread(() =>
        {
            foreach (XamlString res in results.Where(res => res.Key.IsNotNullOrEmptyString()))
                DataGridItemsSource.Add(res);

            results = DataGridItemsSource.ToList();
            int files = DataGridItemsSource.Select(x => x.XamlEntry.AbsolutePath).Distinct().Count();
            SetThreadsInfo($"{DataGridItemsSource.Count} strings in {files} XAML files to update");
        });
    }

    private async Task GetXamlContentAsync(XamlFile file, bool notifyProgress = false)
    {
        SetCurrItemInfo(file.Name);
        await file.LoadXamlContentAsync(Tags, Excludes);

        if (notifyProgress)
            PrgAdd();
    }

    private async Task FormatCsFileAsync(Document doc)
    {
        try
        {
            Document formatted = await Formatter.FormatAsync(doc);
            formatted = await formatted.SortImportDirectivesAsync();
            SourceText csText = await formatted.GetTextAsync();
            var csCode = csText.ToString();
            string ret = csCode.NormalizeLineBreaks();
#if NETFRAMEWORK
            File.WriteAllText(formatted.FilePath!, ret, Encoding.UTF8);
#else
            await File.WriteAllTextAsync(formatted.FilePath!, ret, Encoding.UTF8);
#endif
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }
        finally
        {
            PrgAdd();
        }
    }
}