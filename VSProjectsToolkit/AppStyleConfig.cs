﻿using FEx.WPFx.Models;

namespace VSProjectsToolkit;

public class AppStyleConfig : AppConfig
{
    public AppStyleConfig()
        : base(splashResourceName: "VSProjectsToolkit.png",
            applicationLogoResourceName: "LogoPng",
            windowIconName: "WindowIcon")
    {
    }
}