﻿using FlakApps.Telerik.WinDesktop;
using FlakEssentials.TelerikEx;
using FlakEssentials.TelerikEx.Enums;

namespace VSProjectsToolkit;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : FlakTelerikApp<DotNetProjectsToolkitContainer>
{
    protected override void ComponentInitialize()
    {
        TelerikControlsHandler.Instance.CurrentTheme = TelerikTheme.VisualStudio2013Dark;
        InitializeComponent();
    }
}