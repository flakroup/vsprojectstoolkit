using DotNetProjectsToolkit.Core;
using FEx.Abstractions.Interfaces;
using FEx.DependencyInjection;
using FEx.DI.Abstractions.Interfaces;
using FEx.Downloader;
using FEx.Fundamentals;
using FEx.Json;
using FEx.Legacy;
using FEx.Logging;
using FEx.MVVM;
using FEx.MVVM.Rx;
using FEx.Platforms;
using FEx.RESXx;
using FEx.Telemetry;
using FEx.WPFx;
using FEx.WPFx.Abstractions.Interfaces;
using FlakApps.Services;
using FlakApps.Wpf;
using FlakEssentials.NuGetEx;
using FlakEssentials.RadTreeViewEx;
using FlakEssentials.RollbarEx;
using FlakEssentials.RollbarEx.Abstractions.Interfaces;
using FlakEssentials.XamlStyler;
using Microsoft.Extensions.DependencyInjection;
using StrongInject;
using StrongInject.Extensions.DependencyInjection;
using Telerik.Windows.Controls;

namespace VSProjectsToolkit;

[RegisterModule(typeof(FExDependencyInjectionModule))]
[RegisterModule(typeof(FExLoggingModule))]
[RegisterModule(typeof(FExJsonModule))]
[RegisterModule(typeof(FExPlatformsModule))]
[RegisterModule(typeof(FExDownloaderModule))]
[RegisterModule(typeof(FExMvvmModule))]
[RegisterModule(typeof(FExMvvmRxModule))]
[RegisterModule(typeof(FExWpfxModule))]
[RegisterModule(typeof(FExRollbarxModule))]
[RegisterModule(typeof(FlakAppsServicesModule))]
[RegisterModule(typeof(FExLegacyModule))]
[Register(typeof(AppRollbarConfig), Scope.SingleInstance, typeof(IFExRollbarConfig), typeof(IFExTelemetryConfig))]
[Register(typeof(MsiProc), typeof(IMsiProc))]
[Register(typeof(UpdateUI), typeof(IUpdateUI))]
[Register(typeof(ProjectsService), typeof(IProjectsService))]
[Register(typeof(TreeHandler), typeof(ITreeHandler))]
[Register(typeof(ResxManager), typeof(IResxManager))]
[Register(typeof(RadTreeViewBuilder), typeof(ITreeViewBuilder<RadTreeViewItem>))]
[Register(typeof(XamlStylerCommon))]
[Register(typeof(NuGetManager))]
[Register(typeof(NuGetLogger<NuGetManager>))]
[Register(typeof(AppContainer), Scope.SingleInstance, typeof(IInitializeModule))]
[Register(typeof(AppStyleConfig), Scope.SingleInstance, typeof(IAppConfig))]
public partial class DotNetProjectsToolkitContainer : FExFundamentalsModule, IFExContainer, IFExDownloaderModule,
    IFExRollbarxModule, IFlakAppsServicesContainer, IFExLegacyContainer, IContainer<IProjectsService>,
    IContainer<ITreeHandler>, IContainer<IResxManager>, IContainer<ITreeViewBuilder<RadTreeViewItem>>,
    IContainer<NuGetManager>, IContainer<NuGetLogger<NuGetManager>>, IContainer<XamlStylerCommon>
{
    public static void AddServices(DotNetProjectsToolkitContainer container, IServiceCollection services)
    {
        services.AddTransientServiceUsingContainer<IMsiProc>(container);
        services.AddTransientServiceUsingContainer<IUpdateUI>(container);
        services.AddTransientServiceUsingContainer<IProjectsService>(container);
        services.AddTransientServiceUsingContainer<ITreeHandler>(container);
        services.AddTransientServiceUsingContainer<IResxManager>(container);
        services.AddTransientServiceUsingContainer<XamlStylerCommon>(container);
        services.AddTransientServiceUsingContainer<ITreeViewBuilder<RadTreeViewItem>>(container);
        services.AddTransientServiceUsingContainer<NuGetManager>(container);
        services.AddTransientServiceUsingContainer<NuGetLogger<NuGetManager>>(container);
    }
}