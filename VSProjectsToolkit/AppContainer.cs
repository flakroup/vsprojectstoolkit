﻿using FEx.DI.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace VSProjectsToolkit;

public class AppContainer : InitializeModule<DotNetProjectsToolkitContainer>
{
    protected override void AddServices(DotNetProjectsToolkitContainer container, IServiceCollection services) =>
        DotNetProjectsToolkitContainer.AddServices(container, services);
}