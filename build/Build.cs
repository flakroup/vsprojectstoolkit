﻿using FlakApps.Build;
using Nuke.Common.Execution;

namespace _build
{
    [UnsetVisualStudioEnvironmentVariables]
    internal class Build : FlakAppBuild
    {
        public static int Main() => Execute<Build>(x => x.Compile);
    }
}